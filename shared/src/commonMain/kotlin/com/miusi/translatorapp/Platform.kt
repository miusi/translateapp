package com.miusi.translatorapp

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform