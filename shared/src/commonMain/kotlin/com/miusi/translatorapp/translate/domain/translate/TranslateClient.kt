package com.miusi.translatorapp.translate.domain.translate

import com.miusi.translatorapp.core.domain.language.Language

interface TranslateClient {
    suspend fun translate(
        fromLanguage: Language,
        fromText: String,
        toLanguage: Language
    ): String
}