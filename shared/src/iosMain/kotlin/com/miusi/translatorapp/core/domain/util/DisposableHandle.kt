package com.miusi.translatorapp.core.domain.util

import kotlinx.coroutines.DisposableHandle

fun interface DisposableHandle : DisposableHandle