package com.miusi.translatorapp.android

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TranslateApp: Application()